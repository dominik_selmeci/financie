<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Výpočet rozdelenia financií</title>

		<link rel="stylesheet" href="/assets/style.css">

	</head>

	<body>
		<div class="container">
			<div class="header clearfix">
				<h3 class="text-muted text-center">Správa financií</h3>
			</div>
			<div class="row">

				<div class="col-lg-4">
					<div class="panel panel-default">
						<div class="panel-heading">Pridanie výdavku do džbánu</div>
						<div class="panel-body">
						   	<div class="row load-outgo">
								
							</div>
						</div>
					</div>		
				</div>

				<div class="col-lg-8">

					<div class="panel panel-default">
					  	<div class="panel-heading">Výpočet rozdelenia výplaty na mesiac</div>
					  	<div class="panel-body">
					    	<p class="bg-success p15 text-center">Zadajte svoju výplatu a program vám prepočíta vaše peniaze medzi jednotlivé džbány.
							Môžete zadať aj výšku vášho platového stropu a v prípade že zarobíte viac tak sa zostatok rozdelí medzi Finančnú slobodu, 
							Dlhodobé sporenie a Zábavu (ako odmenu za zvýšenie platu ;)</p>

							<div class="row">
								<form class="center-block form-horizontal">
									<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
										<input type="text" name="salary" class="form-control" value="" placeholder="- zadajte plat -">
									</div>

									<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
										<input type="text" name="salary-cap" class="form-control" value="" placeholder="- platový strop -">
									</div>

									<div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
										<button type="button" class="btn btn-primary col-xs-12 col-sm-12 col-md-12 col-lg-12 but-calculate">Vypočítaj</button>
									</div>
								</form>

								<div class="load-pitchers"></div>
							</div>
					  	</div>
					</div>
								
				</div>
					
			</div>
		</div>


		<!-- Modal -->
		<div class="modal fade" id="cost-dialog" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">title</h4>
					</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-12 success-message">
							<p class="bg-success p15 text-center">Výdavok bol úspešne uložený</p>
						</div>
						<form class="center-block">
							<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
								<div class="form-group">
							    	<label for="cost">Názov výdavku:</label><br>
							    	<input type="text" name="cost-title" class="form-control" value="" placeholder="- názov -">
								</div>
							</div>
							
							<div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
								<div class="form-group">
							    	<label for="cost">Zadajte sumu (&euro;):</label>
							    	<input type="text" name="cost" class="form-control" value="" placeholder="- suma -">
								</div>
							</div>
						</form>
					</div>
						
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary save-cost">Uložiť výdavok (Enter)</button>
				</div>
				</div>
			</div>
		</div>

		<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
		<script src="/assets/lib/bootstrap/js/bootstrap.min.js"></script>
		<script src="/assets/lib/typehead/typeahead.bundle.min.js"></script>
		<script type="text/javascript" src="/assets/js/main.js"></script>
	</body>

</html>