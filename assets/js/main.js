/*
 * Verzia 1.0
 * 
 * Návrhy a vylepšenia (Verzia 1.1)
 * --------------------------------------------------------------
 * - doplniť pekný vzhľad - implementovať bootstrap
 */
(function($){

	/*
	 * Jednotlivé džbány
	 */
	var pitchers = [];

	/*
	 * Výpočet jednotlivých položiek
	 */
	function calculate_items(pitchers)
	{
		//výplata
        var salary = parseFloat($('input[name=salary]').val());

        //platový strop - pre výpočet položiek, ktoré sú obmedzené platovým stropom
        var salary_cap = parseFloat($('input[name="salary-cap"]').val());

        //zostatok
        var balance = salary; 

        //súčet percent, položiek neobmedzených platovým stropom
        var percent_sum = 0;

        //ukladanie do html
        var html = '';

        //počet džbánov
        var n = pitchers.length;

        if (salary_cap && salary > salary_cap) //ak je zadaný platový strop
        {
        	//rozdelenie s obmedzením na platový strop
        	for (var i=0; i<n; i++) {
        		if (pitchers[i].salary_cap) {
        			pitchers[i].pay = salary_cap * pitchers[i].percent;
        			balance -= pitchers[i].pay;
        		} else {
        			percent_sum += pitchers[i].percent;
        		}
        	}

        	//prepočet zostatku na položky neobmedzené platovým stropom
        	for (var i=0; i<n; i++) {
        		if (!pitchers[i].salary_cap) {
        			pitchers[i].pay = balance * (pitchers[i].percent/percent_sum);
        		}
        	}
        }
        else //ak nie je zadaný platový strop
        {
        	for (var i=0; i<n; i++) {
				pitchers[i].pay = salary * pitchers[i].percent;
        	}
        }

        for (var i=0; i<n; i++)
        {
        	html += '<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">'+
					'<div class="bg-danger pitcher">'+
						'<h4 class="text-center">'+pitchers[i].title+'<br><strong>'+(pitchers[i].percent*100)+'%</strong></h4>'+
						'<h2 class="text-center">'+(pitchers[i].pay).toFixed(2)+' &euro;</h2>'+
						'<p class="small text-muted text-center"><em>'+pitchers[i].description+'</em></p>'+
					'</div>'+
				'</div>';
        }
        

		$('.load-pitchers').html(html);
	}

	function loadPitchers(pitchers)
	{
		var html = '';

		for (var i in pitchers)
		{
			var pitcher = pitchers[i];

			html += '<div class="col-md-4 pitcher-balance" data-toggle="modal" data-target="#cost-dialog" data-id="'+pitcher.id+'">';
				html += '<img src="https://openclipart.org/download/7415/johnny-automatic-vase.svg" />';
				html += '<span class="info-balance">'+(pitcher.salary_total - pitcher.expense_total)+' &euro;</span>';
				html += '<p class="text-center"><strong>'+pitcher.title+'</strong></p>';
			html += '</div>';
		}

		$('.load-outgo').html(html);
	}

	function loadAutocomplete()
	{
		var substringMatcher = function(strs) {
			return function findMatches(q, cb) {
				var matches, substringRegex;

				// an array that will be populated with substring matches
				matches = [];

				// regex used to determine if a string contains the substring `q`
				substrRegex = new RegExp(q, 'i');

				// iterate through the pool of strings and for any string that
				// contains the substring `q`, add it to the `matches` array
				$.each(strs, function(i, str) {
					if (substrRegex.test(str)) {
						matches.push(str);
					}
				});

				cb(matches);
			};
		};

		jQuery.getJSON('/ajax.php/getExpenseTitles', function(titles) {
		  	$('#cost-dialog input[name=cost-title]').typeahead({
				hint: true,
				highlight: true,
				minLength: 0
			},
			{
				name: 'titles',
				source: substringMatcher(titles)
			});

		});
	}

	function saveCost()
	{
		var id = parseInt($('#cost-dialog').data('id'));
		var price = parseFloat(($('#cost-dialog input[name=cost]').val()).replace(',','.'));
		var title = $('#cost-dialog input[name=cost-title]').val();

		if (price <= 0 || isNaN(price)) {
			alert('Zadajte nenulovú cenu'); 
			$('#cost-dialog input[name=cost]').html('').focus();
			return;
		}

		if (title.length <= 0) {
			alert('Zadajte názov výdavku'); 
			$('#cost-dialog input[name=cost-title]').html('').focus();
			return;
		}

		$.post('/ajax.php/saveCost', {
			id: id,
			price: price,
			title: title
		}, function(data) {
			loadAutocomplete();
			initPitchers();
			$('#cost-dialog input[name=cost]').val('');
			$('#cost-dialog input[name=cost-title]').val('').focus();

			$("#cost-dialog .success-message").show().delay(1500).fadeOut(500);
		});
	}

	function initPitchers()
	{
		$.getJSON('/ajax.php/getPitchers', function(data) {

			pitchers = [];

			for (var i in data)
			{
				pitchers.push({
					id: data[i].pitcher_id,
					title: data[i].pitcher_title,
					percent: data[i].pitcher_percent,
					description: data[i].pitcher_description,
					salary_cap: (data[i].pitcher_salary_cap === 1) ? true : false,
					salary_total: data[i].pitcher_salary_total,
					expense_total: data[i].pitcher_expense_total
				});
			}

			loadPitchers(pitchers);
		});
	}

	$(document).keypress(function(e) {
	    if(e.which == 13 && ($('input[name=salary]').is(':focus') || $('input[name=salary-cap]').is(':focus')) ) {
	    	calculate_items(pitchers);
	    }

	    if(e.which == 13 && ($('input[name=cost]').is(':focus') || $('input[name=cost-title]').is(':focus')) ) {
	    	saveCost();
	    }
	});

	$('.but-calculate').click(function(){
		calculate_items(pitchers);
	});

	$("form").submit(function() {
		return false;
	});

	$('body').on('click', '.pitcher-balance', function() {
		var id = $(this).data('id');

		$('#cost-dialog .modal-title').html('Pridanie výdavku - '+pitchers[id].title);
		$('#cost-dialog').data('id', id);
	});

	$('#cost-dialog').on('shown.bs.modal', function() {
	  	$('#cost-dialog input[name=cost-title]').focus();
	})

	$('#cost-dialog .save-cost').click(function(){
		saveCost();
	});

	$(document).ready(function() 
	{
		initPitchers();
		loadAutocomplete();
	});

}(jQuery));