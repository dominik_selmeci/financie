<?php 

function connectDB()
{
	require_once '/system/dibi/dibi.php';
	
	dibi::connect(array(
	    'driver'   => 'mysql',
	    'host'     => 'mariadb55.websupport.sk:3310',
	    'username' => 'md4kc58s',
	    'password' => '4f15d.cV523',
	    'database' => 'md4kc58s',
	    'charset'  => 'utf8',
	));
}


function pa($array)
{
	echo '<pre>';
	print_r($array);
	echo '</pre>';
}


function saveCost()
{
	$id = (int) $_POST['id'];
	$price = (float) $_POST['price'];
	$title = $_POST['title'];

	if ($price > 0 && strlen($title) > 0)
	{
		$data = [
			'expense_pitcher_id' => $id,
			'expense_price' => $price,
			'expense_title' => $title,
			'expense_created' => date('Y-m-d H:i:s')
		];

		connectDB();
		dibi::insert('expense', $data)->execute();
	}
}


function getExpenseTitles()
{
	connectDB();

	$titles = dibi::select('DISTINCT expense_title')
					->from('expense')
					->fetchPairs();

	echo json_encode($titles);
}


function getPitchers()
{
	connectDB();

	$pitchers = dibi::select('*')
					->from('pitcher')
					->fetchAll();

	foreach ($pitchers as &$pitcher)
	{
		$pitcher['pitcher_salary_total'] = (float) dibi::select('SUM(pitcher_salary_sum)')
												->from('pitcher_salary')
												->where('pitcher_salary_pitcher_id = %i', $pitcher['pitcher_id'])
												->fetchSingle();

		$pitcher['pitcher_expense_total'] = (float) dibi::select('SUM(expense_price)')
													->from('expense')
													->where('expense_pitcher_id = %i', $pitcher['pitcher_id'])
													->fetchSingle();
	}

	echo json_encode($pitchers);
}



/**
 * Zavolanie správnej funkcie podľa uri
 */
$uri = explode('/', $_SERVER['REQUEST_URI']);
$method_name = htmlspecialchars($uri[count($uri)-1]);

call_user_func($method_name);